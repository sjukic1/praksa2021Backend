module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "root",
    DB: "test",
    dialect: "postgres",
    pool: {
        max: 10,
        min: 0,
        idle: 10000,
    }
}