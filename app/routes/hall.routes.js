module.exports = app =>{
    const hall = require('../controllers/hall.controller.js');
    const router = require('express').Router();

    router.post('/hall', hall.create);
    router.put('/hall', hall.createTutorialHall);

    app.use('/api',router);
}