module.exports = app =>{
    const assistant = require('../controllers/assistant.model.js');
    const router = require('express').Router();

    router.post('/assistants', assistant.create);
    router.get('/assistants/:id', assistant.findOne);
    router.patch('/assistants/:id', assistant.update);

    app.use('/api',router);
}