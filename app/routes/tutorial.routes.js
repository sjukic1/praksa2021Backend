module.exports = app =>{
    const tutorials = require('../controllers/tutorial.controller.js');
    const router = require('express').Router();

    router.post('/tutorials', tutorials.create);
    router.get('/tutorials', tutorials.findAll);
    router.get('/tutorials/:id', tutorials.findOne);
    router.patch('/tutorials/:id', tutorials.update);
    router.delete('/tutorials/:id', tutorials.delete);
    router.delete('/tutorials', tutorials.deleteLike);

    app.use('/api',router);
}