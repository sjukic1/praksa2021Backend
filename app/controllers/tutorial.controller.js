const db = require("../models");

const Tutorial = db.tutorials;
const Assistant = db.assistants;
const Op = db.Sequelize.Op;

exports.create = async(req, res) => {
  if (!req.body.title || !req.body.description) {
    res.status(400).json({ message: "Bad request!" });
    return;
  }
  //const assistant = await Assistant.findByPk(req.body.assistantId);
  //if(assistant){
  const tutorial = {
    title: req.body.title,
    description: req.body.description,
  };
  Tutorial.create(tutorial)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({ error: err });
    });
    //}
    //else{
    //    res.status(202).json({message:"Assistant doenst exist!"});
    //}
};

exports.findAll = (req, res) => {
  const title = req.query.title;
  if (!title) {
    res.status(400).json({ message: "Bad request!" });
    return;
  }
  var condition = { title: { [Op.iLike]: `%${title}%` } };
  Tutorial.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({ error: err });
    });
};

exports.findOne = async (req, res) => {
  const id = req.params.id;
  if (!id) {
    res.status(400).json({ message: "Bad request!" });
    return;
  }
  /*const assistant = await Assistant.findAll({where: {tutorialId:id}});
  Tutorial.findByPk(id)
    .then((data) => {
        data = {...data.dataValues, assistants : assistant};
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({ error: err });
    });*/
    Tutorial.findByPk(id, {include : ['assistants']}).then((data)=>{
        res.status(200).json(data);
    })
};

exports.update = (req, res) => {
  const id = req.params.id;
  if (!id) {
    res.status(400).json({ message: "Bad request!" });
    return;
  }
  Tutorial.update(req.body, {
    where: { id: id },
  }).then(data=>{
      if(data.length === 1){
          res.send({message : `Sucessufully updated ${data} records!`});
      }
      else{
        res.send({message : "Unsucessuful update!"});
      }
  }).catch((err)=>{
    res.status(500).send({ error: err });
  });
};

exports.delete = (req, res) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).json({ message: "Bad request!" });
      return;
    }
    Tutorial.destroy({ where: { id: id },
    }).then(data=>{
        if(data === 1){
            res.send({message : `Sucessufully delete ${data} records!`});
        }
        else{
          res.send({message : "Unsucessuful delete!"});
        }
    }).catch((err)=>{
      res.status(500).send({ error: err });
    });
  };

  exports.deleteLike = (req, res)=>{
      const title = req.query.title;
      if(!title){
        res.status(400).json({ message: "Bad request!" });
        return;
      }
    const condition = { title: { [Op.iLike]: `_${title}_` } };
    Tutorial.destroy({where : condition}).then((data)=>{
        if(data === 1){
            res.send({message : `Sucessufully delete ${data} records!`});
        }
        else{
          res.send({message : "Unsucessuful delete!"});
        }
    }).catch((err)=>{
        res.status(500).send({ error: err });
    });
  };