const db = require("../models");

const Hall = db.halls;
const Tutorial = db.tutorials;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).json({ message: "Bad request!" });
        return;
    }
    const hall = {
        name: req.body.name,
    }
    Hall.create(hall).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        res.status(500).josn({error:err});
    })
}


exports.createTutorialHall = async(req,res)=>{
    try{
        const hall = await Hall.findByPk(req.query.idhall);
        const tutorial = await Tutorial.findByPk(req.query.idtut);
        hall.addTutorial(tutorial);
        res.send(hall);
    }
    catch(err){
        res.status(500).json({error:err});
    }
}