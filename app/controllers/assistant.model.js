const db = require("../models");

const Assistant = db.assistants;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    if (!req.body.name || !req.body.age) {
      res.status(400).json({ message: "Bad request!" });
      return;
    }
    const assistant = {
      name: req.body.name,
      age: req.body.age,
      tutorialId: req.body.tutorialId
    };
    Assistant.create(assistant)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({ error: err });
      });
  };

  exports.findOne = (req, res) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).json({ message: "Bad request!" });
      return;
    }
    Assistant.findByPk(id)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({ error: err });
      });
  };

  exports.update = (req, res) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).json({ message: "Bad request!" });
      return;
    }
    Assistant.update(req.body, {
      where: { id: id },
    }).then(data=>{
        if(data.length === 1){
            res.send({message : `Sucessufully updated ${data} records!`});
        }
        else{
          res.send({message : "Unsucessuful update!"});
        }
    }).catch((err)=>{
      res.status(500).send({ error: err });
    });
  };