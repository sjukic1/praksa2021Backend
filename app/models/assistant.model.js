module.exports = (sequelize, Sequelize) => {
    const Assistant = sequelize.define("assistant", {
      name: {
        type: Sequelize.STRING,
      },
      age: {
        type: Sequelize.INTEGER,
      },
    });
    return Assistant;
  };
  