module.exports = (sequelize, Sequelize) => {
    const Hall = sequelize.define("hall", {
      name: {
        type: Sequelize.STRING,
      },
    });
    return Hall;
  };