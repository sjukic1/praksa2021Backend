const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    idel: dbConfig.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.tutorials = require('./tutorial.model.js')(sequelize,Sequelize);
db.assistants = require('./assistant.model.js')(sequelize,Sequelize);
db.halls = require('./hall.model.js')(sequelize,Sequelize);

//db.tutorials.belongsTo(db.assistants);

db.tutorials.hasMany(db.assistants);

db.halls.belongsToMany(db.tutorials, {
    through: "halls_tutorials",
    foreignKey: "hallsId",
});

db.tutorials.belongsToMany(db.halls, {
    through: "halls_tutorials",
    foreignKey: "tutorialsId",
})

module.exports = db;