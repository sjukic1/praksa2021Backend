const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const db = require('./app/models');

const app = express();
const corsOptions = { origin: "http://localhost:8081" };

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended : true }));


require('./app/routes/tutorial.routes.js')(app);
require('./app/routes/assistant.routes.js')(app);
require('./app/routes/hall.routes.js')(app);

app.listen(8000);
app.get('/',(req,res)=>{
    res.json({message: "Test"});
});

const port = process.env.PORT || 8080;

db.sequelize.sync(/*{force:true}*/);

app.listen(port,()=>{
    console.log("Server started succesufully!");
})